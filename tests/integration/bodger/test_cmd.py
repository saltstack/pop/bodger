import pytest


def test_put(hub):
    item = "echo test"
    hub.bodger.cmd.put(item)
    assert item in hub.bodger.RUNS.queue


def test_match(hub):
    hub.bodger.cmd.match()


@pytest.mark.asyncio
async def test_run_all(hub):
    await hub.bodger.cmd.run_all()
